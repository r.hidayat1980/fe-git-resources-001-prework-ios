

## Resources

- [Try Git](http://try.github.io/levels/1/challenges/1)
- [Official Git Videos](http://git-scm.com/videos)
- [Code School GIT Course](https://www.codeschool.com/courses/try-git)
- [Tower's - GIT Cheatsheet (PNG)](http://www.git-tower.com/blog/content/posts/54-git-cheat-sheet/git-cheat-sheet-large01.png)
- [Atlassian's - GIT Cheatsheet (PDF)](https://www.atlassian.com/dms/wac/images/landing/git/atlassian_git_cheatsheet.pdf)
- [Free Github Course on Udacity](https://www.udacity.com/course/ud775)

<p data-visibility='hidden'>View <a href='https://learn.co/lessons/fe-git-resources' title='Resources'>Resources</a> on Learn.co and start learning to code for free.</p>
